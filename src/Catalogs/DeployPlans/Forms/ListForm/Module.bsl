
&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
	
	SetStepsManagementAvailability(True, False);	
	UserInterface.SetInterfaceRestrictionsForSteps(ThisForm, "Steps");	
		
	PathToExecutable1CFile = Constants.PathToExecutable1CFile.Get();
	BackupSharedFolder = Constants.BackupSharedFolder.Get();
	Common1CLogin = Constants.Common1CLogin.Get();
	Common1CPassword = Constants.Common1CPassword.Get();
	UnlockCodeFor1CDatabase = Constants.UnlockCodeFor1CDatabase.Get();
	
	NewMap = New Map;
	MapBackgoundJobsIdsToExecutingStepsAddress = PutToTempStorage(NewMap, ThisForm.UUID);
	
EndProcedure


&AtServer
Procedure SetStepsManagementAvailability(DisableAll, SelectedPlanIsRunning)
		
    Items.GroupRelatedObjects.Enabled = Not DisableAll;
	Items.StepsCancelExecution.Enabled = SelectedPlanIsRunning And Not DisableAll;
	Items.MarkAllSteps.Enabled = Not SelectedPlanIsRunning And Not DisableAll;
	Items.UnmarkAllSteps.Enabled = Not SelectedPlanIsRunning And Not DisableAll;
	Items.ExecutePlan.Enabled = Not SelectedPlanIsRunning And Not DisableAll;
	Items.UpdateStepsFromPlan.Enabled = Not SelectedPlanIsRunning And Not DisableAll;
	Items.Steps.Enabled = Not SelectedPlanIsRunning And Not DisableAll;
	
EndProcedure

	

&AtServer
Procedure AddOutput(Message, Success, Time = Undefined)
	
	NewOutputLine = Output.Add();
	NewOutputLine.Time = ?(Time = Undefined, CurrentDate(), Time);
	NewOutputLine.Message = Message;
	NewOutputLine.Success = Success;
	Items.Output.CurrentRow = NewOutputLine.GetID();	
	
EndProcedure


&AtServer
Procedure CheckFieldsFilledCorrectly()
	
	
	If Not ValueIsFilled(UnlockCodeFor1CDatabase) Then
		CurrentItem = Items.UnlockCodeFor1CDatabase;
		Raise NStr("en = 'Unlock code for operations with 1C databases must be specified'; ru = 'Должен быть указан код разблокировки для операциями над базами 1С'");
	EndIf;		
	
	
	If Not ValueIsFilled(PathToExecutable1CFile) Then
		CurrentItem = Items.PathToExecutable1CFile;
		Raise NStr("en = 'Path to executable 1C file must be specified'; ru = 'Необходимо задать путь к исполняемому файлу 1С'");
	EndIf;		
	
	
	If Not ValueIsFilled(BackupSharedFolder) Then
		CurrentItem = Items.BackupSharedFolder;
		Raise NStr("en = 'Backup shared folder in UNC format must be specified'; ru = 'Необходимо задать путь к сетевому каталогу бэкапов в формате UNC'");
	EndIf;
	
	
	File = New File(PathToExecutable1CFile);	
	
	If Not File.Exist() Then
		CurrentItem = Items.PathToExecutable1CFile;
		Raise NStr("en = 'Specified executable 1C file doesn''t exist or is not available'; ru = 'Указанный исполняемый файл 1С не существует или недоступен'");
	EndIf;	
	
	
	File = New File(BackupSharedFolder);
	
	If Not File.Exist() Or Not File.IsDirectory() Then
		CurrentItem = Items.BackupSharedFolder;
		Raise NStr("en = 'Specified backup shared folder doesn''t exist or is not available'; ru = 'Указанный сетевой каталог бэкапов не существует или недоступен'");
	EndIf;
	
	
	// trying to write and delete file form backup directory
	DirIsWritable = True;
	
	Try
		FullPathToTestFile = BackupSharedFolder + GetPathSeparator() + "TempFileWriteDeleteTest.txt";
		TextFile = New TextDocument;
		TextFile.Write(FullPathToTestFile);
		DeleteFiles(FullPathToTestFile);
	Except
		DirIsWritable = False;
	EndTry;
	
	If Not DirIsWritable Then
		Raise NStr("en = 'Failed to write or delete test file to specified backup shared folder. Maybe it''s not writable.'; ru = 'Не удалось записать тестовый файл в указанный сетевой каталог бэкапов или удалить его. Возможно запись в каталог недоступна.'");		
	EndIf;
	
	
	Cancel = False;
	UserInterface.CheckStepsFileedCorrectly(Undefined, Steps, Cancel);
	
	If Cancel Then
		Raise NStr("en = 'Steps filled incorrectly'; ru = 'Шаги заполнены неверно'");		
	EndIf;
	
	
	
EndProcedure


&AtClient
Procedure ListOnActivateRow(Item)
	
	If LastSelectedPlan = Items.List.CurrentRow Then		
		Return;
	EndIf;
		
	AttachIdleHandler("UpdateLog", 0.5, True);	
		
EndProcedure


&AtClient
Procedure UpdateStepsFromPlan(Command)
	
	LastSelectedPlan = Undefined;
	AttachIdleHandler("UpdateLog", 0.1, True);
	
EndProcedure

&AtClient
Procedure MarkAllSteps(Command)
	SetMarkForAllSteps(True);
EndProcedure

&AtClient
Procedure UnmarkAllSteps(Command)
	SetMarkForAllSteps(False);
EndProcedure

&AtClient
Procedure SetMarkForAllSteps(Value)
	
	For Each Step In Steps Do
		Step.Execute = Value;
	EndDo;
	
EndProcedure




&AtClient
Procedure ExecutePlan(Command)
	
	DeployPlanRef = Items.List.CurrentRow;
	
	If Not ValueIsFilled(DeployPlanRef) Then
		Raise NStr("ru = 'Для выполнения плана сначала необходимо выбрать план'; en = 'To execute plan you must select it first'");
	EndIf;	
	
	CheckFieldsFilledCorrectly();	
	
	ExecutePlanAtServer(DeployPlanRef);
	AttachIdleHandler("UpdateLog", 1, True); // 0.1 is too short period of time to start background job. 0.5 either is not enough. Using 1 second.
		
EndProcedure

&AtServer
Function ExecutePlanAtServer(SelectedDeployPlanRef)
	
	
	ParamSteps = Steps.Unload();
	TempStorageAddress = PutToTempStorage(ParamSteps, ThisForm.UUID);
	
	LastSelectedPlanBackgroundJobID = Processor.ExecutePlanInBackgroundJob(SelectedDeployPlanRef, TempStorageAddress, DebugMode);	
	LastSelectedPlanIsRunning = True;	
	
	Map = GetFromTempStorage(MapBackgoundJobsIdsToExecutingStepsAddress);
	Map.Insert(LastSelectedPlanBackgroundJobID, TempStorageAddress);
	PutToTempStorage(Map, MapBackgoundJobsIdsToExecutingStepsAddress);
		
	Output.Clear();
	
EndFunction


&AtClient
Procedure UpdateLog() Export
	
	NewPlanRowJustActivated = LastSelectedPlan <> Items.List.CurrentRow;	
	CallUpdateLogAtServer = LastSelectedPlanIsRunning Or NewPlanRowJustActivated;
	
	If CallUpdateLogAtServer Then
		LastSelectedPlan = Items.List.CurrentRow;
		UpdateLogAtServer(NewPlanRowJustActivated);
		NewPlanRowJustActivated = False;
		
		If LastSelectedPlanIsRunning Then
			AttachIdleHandler("UpdateLog", 1, True);
		EndIf;
		
	EndIf;
		
EndProcedure	

&AtServer
Procedure UpdateLogAtServer(NewPlanRowJustActivated) Export	
	
	If NewPlanRowJustActivated Then 
		Output.Clear();
		Steps.Clear();
	EndIf;
	
	
	If LastSelectedPlan.IsFolder Then	
		SetStepsManagementAvailability(True, False);
		Return;		
	EndIf;
	
	
	Query = New Query;
	Query.Text = 
		"SELECT
		|	DeployPlansStatesSliceLast.SessionID AS SessionID,
		|	DeployPlansStatesSliceLast.BackgroundJobID AS BackgroundJobID,
		|	DeployPlansStatesSliceLast.EndTime AS EndTime
		|FROM
		|	InformationRegister.DeployPlansStates.SliceLast(, DeployPlan = &DeployPlan) AS DeployPlansStatesSliceLast";
	
	Query.SetParameter("DeployPlan", LastSelectedPlan);
	QueryResult = Query.Execute();
	
	LastSelectedPlanIsRunning = False;
	LastSelectedPlanBackgroundJobID = "";
			
	Selection = QueryResult.Select();
	

	If Selection.Next() Then
		
		LastSelectedPlanBackgroundJobID = Selection.BackgroundJobID;
		LastSelectedPlanIsRunning = Not ValueIsFilled(Selection.EndTime);
		
		LogRecords = Processor.LogRecordsByBackgroundJobID(LastSelectedPlanBackgroundJobID, Not NewPlanRowJustActivated);
		
		For Each Record In LogRecords Do
			AddOutput(Record.Message, Record.Success, Record.Time);
		EndDo;
		
	EndIf;
	
	
	Map = GetFromTempStorage(MapBackgoundJobsIdsToExecutingStepsAddress);
	
	
	If Not LastSelectedPlanIsRunning Then
		
		StoredStepsAsBackgroundJobParam = Map.Get(LastSelectedPlanBackgroundJobID);
		
		If StoredStepsAsBackgroundJobParam <> Undefined Then
			DeleteFromTempStorage(StoredStepsAsBackgroundJobParam);			
			Map.Delete(LastSelectedPlanBackgroundJobID);
			PutToTempStorage(Map, MapBackgoundJobsIdsToExecutingStepsAddress); 
		EndIf;
		
	EndIf;
	
	
	If NewPlanRowJustActivated Then
	
		If LastSelectedPlanIsRunning Then // Loading steps from saved backgournd jop parameters
				
			StoredStepsAsBackgroundJobParam = Map.Get(LastSelectedPlanBackgroundJobID);
			
			If StoredStepsAsBackgroundJobParam <> Undefined Then
				Steps.Load(GetFromTempStorage(StoredStepsAsBackgroundJobParam));
			EndIf;
			
		Else // Loading steps form plan
			
			Steps.Load(LastSelectedPlan.Steps.Unload());
			
			For Each Row In Steps Do
				Row.Execute = True;
			EndDo;
			
		EndIf;
		
	EndIf;		
	
	
	SetStepsManagementAvailability(False, LastSelectedPlanIsRunning);
		
EndProcedure	




&AtClient
Procedure ShowPasswordsOnChange(Item)
	
	Items.Common1CPassword.PasswordMode = Not ShowPasswords;
	
EndProcedure



&AtClient
Procedure PathToExecutable1CFileOnChange(Item)
	PathToExecutable1CFileOnChangeAtServer();
EndProcedure

&AtServer
Procedure PathToExecutable1CFileOnChangeAtServer()
	Constants.PathToExecutable1CFile.Set(PathToExecutable1CFile);
EndProcedure


&AtClient
Procedure BackupSharedFolderOnChange(Item)
	BackupSharedFolderOnChangeAtServer();
EndProcedure

&AtServer
Procedure BackupSharedFolderOnChangeAtServer()
	Constants.BackupSharedFolder.Set(BackupSharedFolder);
EndProcedure                                                           



&AtClient
Procedure Common1CLoginOnChange(Item)
	Common1CLoginOnChangeAtServer();
EndProcedure


&AtServer
Procedure Common1CLoginOnChangeAtServer()
	Constants.Common1CLogin.Set(Common1CLogin);
EndProcedure


&AtClient
Procedure Common1CPasswordOnChange(Item)
	Common1CPasswordOnChangeAtServer();
EndProcedure


&AtServer
Procedure Common1CPasswordOnChangeAtServer()
	Constants.Common1CPassword.Set(Common1CPassword);
EndProcedure


&AtClient
Procedure UnlockCodeFor1CDatabaseOnChange(Item)
	UnlockCodeFor1CDatabaseOnChangeAtServer();
EndProcedure

&AtServer
Procedure UnlockCodeFor1CDatabaseOnChangeAtServer()
	Constants.UnlockCodeFor1CDatabase.Set(UnlockCodeFor1CDatabase);
EndProcedure



&AtClient
Procedure CreateGroup(Command)
	OpenForm("Catalog.DeployPlans.FolderForm", New Structure("IsFolder", True));
EndProcedure

&AtClient
Procedure BeforeClose(Cancel, Exit, MessageText, StandardProcessing)
	If Not Exit Then
		Cancel = True;
	EndIf;
EndProcedure

&AtClient
Procedure CancelExecution(Command)
	LastSelectedPlan = Undefined;
	AttachIdleHandler("UpdateLog", 1, True);
	CancelExecutionAtServer();	
EndProcedure

&AtServer
Procedure CancelExecutionAtServer()
	
	JobID = LastSelectedPlanBackgroundJobID;
	JobFound = Processor.TerminatePlanExecution(JobID);
	
	LastSelectedPlanBackgroundJobID = "";	
	
	SetStepsManagementAvailability(False, False);
	
	
	If Not JobFound Then 
		Message = NStr("en = 'Background job was not found by identifier %1. It was stopped later.'; ru = 'Фоновое задание с идентификатором %1 не было найдено. Оно было остановелно ранее.'");
		Message = StrTemplate(Message, JobID);
		Raise Message;
	EndIf;		
			
EndProcedure

&AtClient
Procedure LogForSelectedPlan(Command)
	
	FormParams = New Structure("Filter", New Structure("DeployPlan", Items.List.CurrentRow));
	OpenForm("InformationRegister.DeployPlansLog.ListForm", FormParams);
	
EndProcedure

&AtClient
Procedure ExecutionStatesForSelectedPlan(Command)
	FormParams = New Structure("Filter", New Structure("DeployPlan", Items.List.CurrentRow));
	OpenForm("InformationRegister.DeployPlansStates.ListForm", FormParams);
EndProcedure

&AtClient
Procedure LogForAllPlans(Command)
	OpenForm("InformationRegister.DeployPlansLog.ListForm");
EndProcedure

&AtClient
Procedure ExecutionStatesForAllPlans(Command)
	OpenForm("InformationRegister.DeployPlansStates.ListForm");
EndProcedure


