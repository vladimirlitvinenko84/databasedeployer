
Procedure BeforeWrite(Cancel)
	
	If ThisObject.IsFolder Then
		Return;
	EndIf;
	
	UniqueIdentifier = TrimAll(UniqueIdentifier);
	
	If ValueIsFilled(UniqueIdentifier) Then
	
		Query = New Query;
		Query.Text = 
			"SELECT TOP 1
			|	DeployPlans.Ref AS Ref,
			|	DeployPlans.Presentation AS Presentation
			|FROM
			|	Catalog.DeployPlans AS DeployPlans
			|WHERE
			|	DeployPlans.Ref <> &Ref And DeployPlans.UniqueIdentifier = &UniqueIdentifier";
		
		Query.SetParameter("Ref", Ref);
		Query.SetParameter("UniqueIdentifier", UniqueIdentifier);
		
		QueryResult = Query.Execute();
		
		If Not QueryResult.IsEmpty() Then
			
			Selection = QueryResult.Select();
			Selection.Next();
			
			MsgTemplate = NStr("en = 'Deploy plan with  identifier %1 is already exists. Plan name: %2'; ru = 'План развертывания с идентификатором %1 уже существует. Имя плана: %2'");
			
			Message = New UserMessage;
			Message.Text = StrTemplate(MsgTemplate, UniqueIdentifier, Selection.Presentation);
			Message.Field = "UniqueIdentifier";
			Message.DataKey = Selection.Ref;
			Message.Message();
			
			Cancel = True;
			
		EndIf;
		
	EndIf;	
	
	
EndProcedure

Procedure FillCheckProcessing(Cancel, CheckedAttributes)
	
	UserInterface.CheckStepsFileedCorrectly(ThisObject, ThisObject.Steps, Cancel);
	
EndProcedure
