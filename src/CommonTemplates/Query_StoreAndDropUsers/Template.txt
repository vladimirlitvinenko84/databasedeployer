USE [%1]
EXEC sp_rename 'v8users', 'v8users_old'
UPDATE Params
	SET FileName = 'users.usr_old'
	WHERE FileName = 'users.usr'
