
Function Connection(Instance, User, Password, Val Database = Undefined) 
	
	
	If Database = Undefined Then
		Database = "master";
	EndIf;

	Connection  = New COMObject("ADODB.Connection");
	Connection.ConnectionTimeout = 30;


	Connection.ConnectionString =
	    "driver={SQL Server};" +
	    "server="+Instance+";"+
	    "uid="+User+";"+
	    "pwd="+Password+";"+
	    "database="+Database+";";

	Connection.Open();

	Return Connection;
		
EndFunction


Function RecordSetFromQuery(Connection, Query) 
	
	Command = New COMObject("ADODB.Command");
	Command.ActiveConnection = Connection;
	Command.CommandTimeout = 0; // indefinitely https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/commandtimeout-property-ado 
	Command.CommandText = Query;
	RecordSet = New COMObject("ADODB.RecordSet");
	RecordSet = Command.Execute(); 
	
	Return RecordSet;
			
EndFunction


Function TableFromRecordSet(RecordSet) 
			
	Table = New ValueTable;
	
	For ColumnIndex = 0 По RecordSet.Fields.Count-1 Do
		NameFiled = RecordSet.Fields.Item(ColumnIndex).Name;
		NameFiled = StrReplace(NameFiled,"$","_");
		Table.Columns.Add(NameFiled,,RecordSet.Fields.Item(ColumnIndex).Name, 15);
	EndDo;
	
	If Not RecordSet.EOF() Then
		
		RecordSet.MoveFirst();                 
		Пока Not RecordSet.EOF() Do
			Columns = Table.Add();
			For ColumnIndex = 0 По RecordSet.Fields.Count-1 Do
				Columns[ColumnIndex] = RecordSet.Fields(RecordSet.Fields.Item(ColumnIndex).Name).Value;
			EndDo;
			RecordSet.MoveNext();  
		EndDo;
		
	EndIf;
	
	Return Table;
		
EndFunction


Procedure ExecuteQuery(Server, User, Password, Query, Database = Undefined) Export
	
	Connection = Connection(Server, User, Password, Database);
					
	RecordSet = RecordSetFromQuery(Connection, Query);
	
	Try
		RecordSet.Close();
	Except
	EndTry;
	
	Try
		Connection.Close();
	Except
	EndTry;
				
EndProcedure


Function ValueTableFromQuery(Server, User, Password, Query, Database = Undefined) Export
	
	Connection = Connection(Server, User, Password, Database);
					
	RecordSet = RecordSetFromQuery(Connection, Query);
	
	Table = Undefined;
	
	Try
		Table = TableFromRecordSet(RecordSet);
	Except
	EndTry;	
	
	Try
		RecordSet.Close();
	Except
	EndTry;
	
	Try
		Connection.Close();
	Except
	EndTry;

	Return Table;
				
EndFunction





