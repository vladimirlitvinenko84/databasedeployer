
Procedure PinErrorMessageToStepsRow(Object, LineNumber, Text, Field, Cancel) 
	
	Cancel = True;
	Message = New UserMessage;	
	Message.Text = Text;
	Message.Field = "Steps["+Format(LineNumber-1,"NG=0")+"]."+Field;
	Message.SetData(Object);
	Message.Message();

EndProcedure

Procedure CheckStepsFileedCorrectly(Object, Steps, Cancel) Export
	
	UserAvailable = UserAvailableActions();
	NeedSource = ActionsThatNeedSourceDatabase();	
	NeedScript = ActionsThatNeedScript();			
	NeedTarget = ActionsThatNeedTargetDatabase();	

	LineNumber = 0;
	
	For Each Step In Steps Do
		
		LineNumber = LineNumber + 1;
				
		If Not ValueIsFilled(Step.Action) Then
			Text = NStr("en = 'Action must be specified on step %1'; ru = 'Для шага %1 необходимо указать действие'");
			Text = StrTemplate(Text, LineNumber);
			PinErrorMessageToStepsRow(Object, LineNumber, Text, "Action", Cancel); 			
		EndIf;
		
		If UserAvailable.Find(Step.Action) = Undefined Then
			Text = NStr("en = 'Action %1 specified on step %2 cannot be used directly'; ru = 'Действие %1, указанное на шаге %2 нельзя использовать напрямую'");
			Text = StrTemplate(Text, Step.Action, LineNumber);
			PinErrorMessageToStepsRow(Object, LineNumber, Text, "Action", Cancel);
		EndIf;
		
		If NeedTarget.Find(Step.Action) <> Undefined And Not ValueIsFilled(Step.TargetDatabase) Then
			Text = NStr("en = 'Target database must be specified for action %1 on step %2'; ru = 'Для действия %1 на шаге %2 должна быть указана целевая база'");
			Text = StrTemplate(Text, Step.Action, LineNumber);
			PinErrorMessageToStepsRow(Object, LineNumber, Text, "TargetDatabase", Cancel);
		EndIf;		
		
		If NeedSource.Find(Step.Action) <> Undefined And Not ValueIsFilled(Step.SourceDatabase) Then
			Text = NStr("en = 'Source database must be specified for action %1 on step %2'; ru = 'Для действия %1 на шаге %2 должна быть указана база-источник'");
			Text = StrTemplate(Text, Step.Action, LineNumber);
			PinErrorMessageToStepsRow(Object, LineNumber, Text, "SourceDatabase", Cancel);
		EndIf;
				
		If NeedScript.Find(Step.Action) <> Undefined And Not ValueIsFilled(Step.Script) Then
			Text = NStr("en = 'Script must be specified for action %1 on step %2'; ru = 'Для действия %1 на шаге %2 должна быть указан скрипт'");
			Text = StrTemplate(Text, Step.Action, LineNumber);
			PinErrorMessageToStepsRow(Object, LineNumber, Text, "Script", Cancel);
		EndIf;
		
		If Step.Action = Enums.Actions.ConfigurationRepositoryBindCfg And ValueIsFilled(Step.TargetDatabase) And Not ValueIsFilled(Step.TargetDatabase.StoragePath) Then
			Text = NStr("en = 'Storage path be specified in database %3 for action %1 on step %2'; ru = 'Для действия %1 на шаге %2 в базе %3 должен быть указан путь к хранилищу'");
			Text = StrTemplate(Text, Step.Action, LineNumber, Step.TargetDatabase);
			PinErrorMessageToStepsRow(Object, LineNumber, Text, "TargetDatabase", Cancel);
		EndIf;
		
		                                                               
	EndDo;
	
EndProcedure


Function UserAvailableActions()
	
	Actions = Enums.Actions;
	Result = New Array;
	Result.Add(Actions.DenySessions);
	Result.Add(Actions.DenyScheduledJobs);
	Result.Add(Actions.TerminateSessions);
	Result.Add(Actions.CopyTargetDatabaseFromSourceDatabase);
	Result.Add(Actions.SetRecoveryModelToSimple);	
	Result.Add(Actions.ShrinkDatabase);	
	Result.Add(Actions.PermitDirtyRead);
	Result.Add(Actions.LoadConfigurationFromSourceDatabase);	
	Result.Add(Actions.RestoreTargetDatabaseFromBackupDirectory);
	Result.Add(Actions.ConfigurationRepositoryBindCfg);
	Result.Add(Actions.PermitSessions);
	Result.Add(Actions.PermitScheduledJobs);
	Result.Add(Actions.ClearObjectVersions);
	// For future realization
	// Result.Add(Actions.ExecuteScriptWithinCOMConnectionToTargetDatabase);
	// Result.Add(Actions.ExecuteScriptWithinDatabaseDeployer);	
	
	Return Result;
	
EndFunction


Function ActionsThatNeedSourceDatabase()
	
	Actions = Enums.Actions;
	Result = New Array;
	Result.Add(Actions.CopyTargetDatabaseFromSourceDatabase);
	Result.Add(Actions.LoadConfigurationFromSourceDatabase);	
	
	Return Result;
	
EndFunction


Function ActionsThatNeedTargetDatabase()
	
	Actions = Enums.Actions;
	Result = New Array;
	Result.Add(Actions.CopyTargetDatabaseFromSourceDatabase);
	Result.Add(Actions.SetRecoveryModelToSimple);	
	Result.Add(Actions.LoadConfigurationFromSourceDatabase);	
	Result.Add(Actions.RestoreTargetDatabaseFromBackupDirectory);	
	Result.Add(Actions.ConfigurationRepositoryBindCfg);	
	Result.Add(Actions.DenySessions);
	Result.Add(Actions.DenyScheduledJobs);
	Result.Add(Actions.TerminateSessions);
	Result.Add(Actions.PermitSessions);
	Result.Add(Actions.PermitScheduledJobs);
	Result.Add(Actions.PermitDirtyRead);
	Result.Add(Actions.ClearObjectVersions);
	Result.Add(Actions.ShrinkDatabase);

	Return Result;
	
EndFunction


Function ActionsThatNeedScript()
	
	Actions = Enums.Actions;
	Result = New Array;
	Result.Add(Actions.ExecuteScriptWithinCOMConnectionToTargetDatabase);
	Result.Add(Actions.ExecuteScriptWithinDatabaseDeployer);	

	Return Result;
	
EndFunction



Procedure SetInterfaceRestrictionsForSteps(Form, PathToAttributes) Export
	
	Form.Items.StepsAction.ListChoiceMode = True;
	
	Actions = Enums.Actions;
	UserActions = UserAvailableActions();
	
	Form.Items.StepsAction.ChoiceList.Clear();
	For Each Action In UserActions Do 
		Form.Items.StepsAction.ChoiceList.Add(Action);
	EndDo;
		
	SetConditionalAppearanceForStepsFields(Form, PathToAttributes, "SourceDatabase", "StepsSourceDatabase", ActionsThatNeedSourceDatabase());	
	SetConditionalAppearanceForStepsFields(Form, PathToAttributes, "Script", "StepsScript", ActionsThatNeedScript());			
	SetConditionalAppearanceForStepsFields(Form, PathToAttributes, "TargetDatabase", "StepsTargetDatabase", ActionsThatNeedTargetDatabase());	
	
EndProcedure




Procedure SetConditionalAppearanceForStepsFields(Form, PathToAttributes, AttributeName, FormItemFoAffect, ArrayOfActionsWhenFieldIsAppliable)

	ListOfActionsWhenFieldIsAppliable = New ValueList;
	ListOfActionsWhenFieldIsAppliable.LoadValues(ArrayOfActionsWhenFieldIsAppliable);
	
	
	AppearanceItem = Form.ConditionalAppearance.Items.Add();
	
	FilterElement = AppearanceItem.Filter.Items.Add(Type("DataCompositionFilterItem"));
	FilterElement.LeftValue = New DataCompositionField(PathToAttributes+".Action");
	FilterElement.ComparisonType = DataCompositionComparisonType.NotInList; 
		
	FilterElement.RightValue = ListOfActionsWhenFieldIsAppliable;
	FilterElement.Use = True;

	AppearanceItem.Appearance.SetParameterValue("TextColor", WebColors.DarkGray);
	AppearanceItem.Appearance.SetParameterValue("ReadOnly", True);
	AppearanceItem.Appearance.SetParameterValue("Text", "<not appliable>");
		
	FieldsToAffect = AppearanceItem.Fields.Items.Add();
	FieldsToAffect.Field = New DataCompositionField(FormItemFoAffect);
	FieldsToAffect.Use = True;	
	
	
	
	AppearanceItem = Form.ConditionalAppearance.Items.Add();
	
	FilterElement = AppearanceItem.Filter.Items.Add(Type("DataCompositionFilterItem"));
	FilterElement.LeftValue = New DataCompositionField(PathToAttributes+".Action");
	FilterElement.ComparisonType = DataCompositionComparisonType.InList; 		
	FilterElement.RightValue = ListOfActionsWhenFieldIsAppliable;
	FilterElement.Use = True;

	FilterElement = AppearanceItem.Filter.Items.Add(Type("DataCompositionFilterItem"));
	FilterElement.LeftValue = New DataCompositionField(PathToAttributes+"." + AttributeName);
	FilterElement.ComparisonType = DataCompositionComparisonType.NotFilled; 		
	FilterElement.Use = True;	
	
	AppearanceItem.Appearance.SetParameterValue("MarkIncomplete", True);
		
	FieldsToAffect = AppearanceItem.Fields.Items.Add();
	FieldsToAffect.Field = New DataCompositionField(FormItemFoAffect);
	FieldsToAffect.Use = True;	
	
	
EndProcedure	