 
Function TextFromTemplate(TemplateName) Export
	
	Text = GetCommonTemplate(TemplateName).GetText();
	Return Text;
	
EndFunction


Procedure BackupDatabase(Params)
	
	File = New File(Params.BackupFile);
	
	If File.Exist() Then
		DeleteFiles(Params.BackupFile);
	EndIf;
	
	QueryText = TextFromTemplate("Query_BackupDatabase");
	QueryText = StrTemplate(QueryText, Params.Database, Params.BackupFile);
	
	SQL.ExecuteQuery(Params.Server, Params.User, Params.Password, QueryText, Params.Database);
	
	If Not File.Exist() Then
		Raise StrTemplate(NStr("ru = 'В результате запроса не сформирован файл %1'; en = 'File %1 was not created as the result of query'"), Params.BackupFile);
	EndIf;
	
	
EndProcedure


Procedure RestoreDatabase(Params)
	
	File = New File(Params.BackupFile);
	
	If Not File.Exist() Then
		Raise StrTemplate(NStr("ru = 'Не обнаружен файл с бэкапом для восстановления : %1'; en = 'Backup file %1 was not found'"), Params.BackupFile);
	EndIf;
	
	
	// https://docs.microsoft.com/ru-ru/sql/t-sql/statements/restore-statements-filelistonly-transact-sql
	// https://docs.microsoft.com/en-us/sql/relational-databases/system-catalog-views/sys-database-files-transact-sql
	//
	// Результат запроса к RESTORE  FILELISTONLY сопоставляем с результатом запроса к sys.database_files
	// Тип D сопоставляем с типом 0 , тип L сопоставляем с типом 1
		
	
	Query_GetBackupFileInfo = TextFromTemplate("Query_GetBackupFileInfo");
	Query_GetBackupFileInfo = StrTemplate(Query_GetBackupFileInfo, Params.BackupFile);
	Table_BackupFileInfo = SQL.ValueTableFromQuery(Params.Server, Params.User, Params.Password, Query_GetBackupFileInfo);
		
	Query_GetDatabaseFilesInfo = TextFromTemplate("Query_GetDatabaseFilesInfo");
	Table_DatabaseFileInfo = SQL.ValueTableFromQuery(Params.Server, Params.User, Params.Password, Query_GetDatabaseFilesInfo, Params.Database);
	
	
	Source_LogicalName_Data = "";
	Source_LogicalName_Log = "";
	
	For Each Row In Table_BackupFileInfo Do
		
		If Upper(String(Row.Type)) = "D" Then
			Source_LogicalName_Data = Row.LogicalName;
		ElsIf Upper(String(Row.Type)) = "L" Then
			Source_LogicalName_Log = Row.LogicalName; 
		EndIf;
		
	EndDo;
	
	If Not ValueIsFilled(Source_LogicalName_Data) Then
		Raise StrTemplate(NStr("ru = 'Не удалось определить логическое имя файла данных из файла бэкапа : %1'; en = 'Faild to get logical name of data file from backup file %1'"), Params.BackupFile);	
	EndIf;
	
	
	If Not ValueIsFilled(Source_LogicalName_Log) Then
		Raise StrTemplate(NStr("ru = 'Не удалось определить логическое имя файла лога из файла бэкапа : %1'; en = 'Faild to get logical name of log file from backup file %1'"), Params.BackupFile);	
	EndIf;
	
	
	
	
	Destination_PhysicalName_Data = "";
	Destination_PhysicalName_Log = "";
	
	For Each Row In Table_DatabaseFileInfo Do
		
		If Number(Row.type) = 0 Then
			Destination_PhysicalName_Data = Row.physical_name;
		ElsIf Number(Row.type) = 1 Then
			Destination_PhysicalName_Log = Row.physical_name;
		EndIf;
		
	EndDo;
	
	If Not ValueIsFilled(Destination_PhysicalName_Data) Then
		Raise StrTemplate(NStr("ru = 'Не удалось определить путь к файлу данных для базы данных %1 на сервере %2'; en = 'Faild to get path to data file for databse %1 on server %2'"), Params.Database, Params.Server);	
	EndIf;
	
	
	If Not ValueIsFilled(Destination_PhysicalName_Log) Then
		Raise StrTemplate(NStr("ru = 'Не удалось определить путь к файлу лога для базы данных %1 на сервере %2'; en = 'Faild to get path to log file for databse %1 on server %2'"), Params.Database, Params.Server);	
	EndIf;
	
		
	
	QueryText = TextFromTemplate("Query_RestoreDatabase");
	QueryText = StrTemplate(QueryText, Params.Database, Params.BackupFile, Source_LogicalName_Data, Destination_PhysicalName_Data, Source_LogicalName_Log, Destination_PhysicalName_Log);
	
	SQL.ExecuteQuery(Params.Server, Params.User, Params.Password, QueryText);
		
EndProcedure


Procedure DeleteFile(BackupFile)
		
	Try
		
		File = New File(BackupFile);
			
		If File.Exist() Then
			
			 DeleteFiles(BackupFile); // doesn't work if file created by domain user that runs ms sql server
			
			//QueryText = TextFromTemplate("Query_DeleteBackupFile");
			//QueryText = StrTemplate(QueryText, Params.BackupFile);
			//SQL.ExecuteQuery(Params.Server, Params.User, Params.Password, QueryText, Params.Database);
					
			If File.Exist() Then
				Raise StrTemplate(NStr("ru = 'Не удалось удалить файл %1'; en = 'Failed to delete file %1'"), BackupFile);
			EndIf;
					
		EndIf;
	
	Except 
		Message(ErrorDescription());
	Endtry;
	
EndProcedure


Function ComConnectionProviders(Server1C, Database1C, User, Password, UnlockCode)

	ConnectionProviders = New Structure("Connector, Connection"); 
	
	ConnectionProviders.Connector = Undefined;
	ConnectionProviders.Connection = Undefined;
	ConnectionString = "Srvr=""%1"";Ref=""%2"";Usr=""%3"";Pwd=""%4"";UC=""%5""";
	ConnectionString = StrTemplate(ConnectionString, Server1C, Database1C, User, Password, UnlockCode);
	
	Connected = False;
	Attempts = 0;
	
	While Attempts <= 20 And Not Connected Do
		
		Try
			
			ConnectionProviders.Connector = New COMObject("V83.COMConnector");
			ConnectionProviders.Connection = ConnectionProviders.Connector.Connect(ConnectionString);			
			Connected = True;
			
		Except
			
			TextError = ErrorDescription();
			
			If StrFind(TextError, "Внешнее соединение не разрешено для указанного пользователя") <> 0
				Or StrFind(TextError, "Неправильное имя или пароль пользователя") <> 0 Then
				
				Raise TextError;
				
			EndIf;	
				
			CloseComConnectionProviders(ConnectionProviders);
			Attempts = Attempts + 1;
			
		Endtry;
		
	EndDo;

	// connecting one more time to raise correct exception
	If Not Connected Then
		ConnectionProviders.Connector = New COMObject("V83.COMConnector");
		ConnectionProviders.Connection =  ConnectionProviders.Connector.Connect(ConnectionString);
	EndIf;
	
	Return ConnectionProviders;
	
EndFunction


Procedure CloseComConnectionProviders(ConnectionProviders)
	
	ConnectionProviders.Connection = Undefined;
	ConnectionProviders.Connector = Undefined;		
	
EndProcedure


Procedure SetRecoveryModelToSimple(Params)
		
	QueryText = TextFromTemplate("Query_SetRecoveryModelToSimple");
	QueryText = StrTemplate(QueryText, Params.Database);
	
	SQL.ExecuteQuery(Params.Server, Params.User, Params.Password, QueryText);	
	
EndProcedure

Procedure PermitDirtyRead(Params)
		
	QueryText = TextFromTemplate("Query_SetReadCommitedSnapshotOff");
	QueryText = StrTemplate(QueryText, Params.Database);
	
	SQL.ExecuteQuery(Params.Server, Params.User, Params.Password, QueryText);	
	
EndProcedure

Procedure ShrinkDatabase(Params)
		
	QueryText = TextFromTemplate("Query_ShrinkDatabases");
	QueryText = StrTemplate(QueryText, Params.Database);
	
	SQL.ExecuteQuery(Params.Server, Params.User, Params.Password, QueryText);	
	
EndProcedure


Procedure RunSystemCommand(CommandString, CheckErrorCode = True)
	
	ReturnCode = 0;
	RunApp(CommandString, , True, ReturnCode);
	
	If ReturnCode <> 0 And CheckErrorCode Then
		Raise "ReturnCode = " + String(ReturnCode);
	EndIf;

EndProcedure


Procedure ConfigurationRepositoryUnbindCfg(Params)
	
	Space = "                                                                                                                                                                          ";
	
	CommandTemplate = """%1"" DESIGNER /DisableStartupDialogs /ConfigurationRepositoryUnbindCfg -force /S""%2\%3"""
					  + Space + Space + Space
					  + "/N ""%4"" /P ""%5"" /UC ""%6""";
		
	CommandString = StrTemplate(CommandTemplate, Params.PathToExecutable1CFile, Params.Server, Params.Database, Params.User, Params.Password, Params.UnlockCode);
	RunSystemCommand(CommandString, False);
	
EndProcedure


Procedure ConfigurationRepositoryBindCfg(Params)
	
	Space = "                                                                                                                                                                     ";
	
	CommandTemplate = """%1"" DESIGNER /DisableStartupDialogs /ConfigurationRepositoryBindCfg -forceBindAlreadyBindedUser -forceReplaceCfg /S""%2\%3"""
					  + Space + Space + Space
					  + "/N ""%4"" /P ""%5"" /UC ""%9"" /ConfigurationRepositoryF ""%6"" /ConfigurationRepositoryN ""%7"" /ConfigurationRepositoryP ""%8""  /UpdateDBCfg -Dynamic-";
		
	CommandString = StrTemplate(CommandTemplate, 
								Params.PathToExecutable1CFile, 
								Params.Server, 
								Params.Database, 
								Params.User, 
								Params.Password, 
								Params.RepositoryPath, 
								Params.RepositoryUser, 
								Params.RepositoryPassword, 
								Params.UnlockCode);
								
	RunSystemCommand(CommandString);
	
EndProcedure


Procedure ConfigurationRepositoryUpdateCfg(Params)
 	
	Space = "                                                                                                                                                                          ";
	
	CommandTemplate = """%1"" DESIGNER /DisableStartupDialogs /ConfigurationRepositoryUpdateCfg -force /S""%2\%3"""
					  + Space + Space + Space
					  + "/N ""%4"" /P ""%5"" /UC ""%9"" /ConfigurationRepositoryF ""%6"" /ConfigurationRepositoryN ""%7"" /ConfigurationRepositoryP ""%8""  /UpdateDBCfg -Dynamic-";
		
	CommandString = StrTemplate(CommandTemplate, 
								Params.PathToExecutable1CFile, 
								Params.Server, 
								Params.Database, 
								Params.User, 
								Params.Password, 
								Params.RepositoryPath, 
								Params.RepositoryUser, 
								Params.RepositoryPassword, 
								Params.UnlockCode);

	RunSystemCommand(CommandString);
	
EndProcedure


Procedure DumpConfigurationFromDatabaseToFile(Params)
	
	Space = "                                                                                                                                                                          ";	
	
	
	CommandTemplate = """%1"" DESIGNER /DisableStartupDialogs /S""%2\%3"" /DumpCfg ""%4"" "
					  + Space + Space + Space
					  + "/N ""%5"" /P ""%6"" /UC ""%7""";

	CommandString = StrTemplate(CommandTemplate, 
								Params.PathToExecutable1CFile, 
								Params.Server, 
								Params.Database, 
								Params.PathToCFFile, 
								Params.User, 
								Params.Password, 
								Params.UnlockCode);
								
								
	RunSystemCommand(CommandString);
	
EndProcedure


Procedure LoadConfigurationFromFile(Params)
	
	Space = "                                                                                                                                                                          ";	
	
	CommandTemplate = """%1"" DESIGNER /DisableStartupDialogs /S""%2\%3"" /LoadCfg ""%4"" /UpdateDBCfg -Dynamic- "
					  + Space + Space + Space
					  + "/N ""%5"" /P ""%6"" /UC ""%7""";
		
	CommandString = StrTemplate(CommandTemplate, 
								Params.PathToExecutable1CFile, 
								Params.Server, 
								Params.Database, 
								Params.PathToCFFile, 
								Params.User, 
								Params.Password, 
								Params.UnlockCode);
								
	RunSystemCommand(CommandString);
	
	
EndProcedure


Procedure UpdateDBCfg(Params)
	
	Space = "                                                                                                                                                                          ";
	
	CommandTemplate = """%1"" DESIGNER /DisableStartupDialogs /S""%2\%3"""
					  + Space + Space + Space
					  + "/N ""%4"" /P ""%5"" /UC ""%6"" /UpdateDBCfg -Dynamic-";
		
	CommandString = StrTemplate(CommandTemplate, Params.PathToExecutable1CFile, Params.Server, Params.Database, Params.User, Params.Password, Params.UnlockCode);
	RunSystemCommand(CommandString);	
	
EndProcedure


Procedure Delay(Params)
	
	StartTime = CurrentDate();
	CurrentTime = StartTime;
	
	While CurrentTime - StartTime < 5 Do
		CurrentTime = CurrentDate();
	EndDo;
	
EndProcedure


Procedure ClearObjectVersions(Params)
		
	ConnectionProviders = ComConnectionProviders(Params.Server1C, Params.Database1C, Params.User1C, Params.Password1C, Params.UnlockCode);
	
	Try
	
		MDArray = ConnectionProviders.Connection.NewObject("Array");
		MDArray.Add(ConnectionProviders.Connection.Metadata.InformationRegisters.ВерсииОбъектов);
		Table = ConnectionProviders.Connection.GetDBStorageStructureInfo(MDArray, True);
		
		
		TablesToTruncate = New Array;
		
		For Each Row In Table Do
			If Row.Назначение = "Основная" Or Row.Назначение = "РегистрацияИзменений" Then
				TablesToTruncate.Add(Row.ИмяТаблицыХранения);
			EndIf;
		EndDo;
		
		CloseComConnectionProviders(ConnectionProviders);	
	
	
		QueryText = "";
		
		For Each TableName In TablesToTruncate Do
			
			QueryText = QueryText 
						+ "TRUNCATE TABLE "
						+ TableName
						+ Chars.LF;		
		EndDo;	
					
		SQL.ExecuteQuery(Params.ServerSQL, Params.UserSQL, Params.PasswordSQL, QueryText, Params.DatabaseSQL);
		
	Except
		
		ErrorText = ErrorDescription();
		CloseComConnectionProviders(ConnectionProviders);
		Raise ErrorText;
		
	EndTry;
		
EndProcedure


// Реализация шагов оставлена как черновик для изменения на новом проекте, текущая реализация не подходит по причине 
// отстутствия реализации методов инициализации тестовых и разработческих баз

//Procedure InitialiseTestDatabase(Params)
//		
//	ConnectionProviders = ComConnectionProviders(Params.Server, Params.Database, Params.User, Params.Password);	
//	ConnectionProviders.Connection.ГП_ОбщегоНазначенияВызовСервера.ИнициализироватьБазуДляРазработкиИТестирования(Params.SettingsFile);
//	CloseComConnectionProviders(ConnectionProviders);	
//	
//EndProcedure


//Procedure StoreSettingsToFile(Params)
//		
//	ConnectionProviders = ComConnectionProviders(Params.Server, Params.Database, Params.User, Params.Password);	
//	ConnectionProviders.Connection.ГП_ОбщегоНазначенияВызовСервера.СохранитьВФайлНастройкиБазыДляРазработкиИТестирования(Params.SettingsFile);
//	CloseComConnectionProviders(ConnectionProviders);	
//	
//EndProcedure



Procedure ExecuteStep(Step) 
	
	Actions = Enums.Actions;
	Action = Step.Action;
	Params = Step.Params;
	
	
	If Action = Actions.Delay Then 
		
		Delay(Params);
		
	ElsIf Action = Actions.BackupDatabaseToTempDir Then
		
		BackupDatabase(Params);
		                                                  
	ElsIf Action = Actions.RestoreDatabaseFromTempDir Then
		
		RestoreDatabase(Params);	
		
	ElsIf Action = Actions.DeleteBackupSqlFile Then
		                            
		DeleteFile(Params.BackupFile);
		
	ElsIf Action = Actions.DeleteBackupCfFile Then
		                            
		DeleteFile(Params.PathToCFFile);
						
	ElsIf Action = Actions.ShrinkDatabase Then
		
		ShrinkDatabase(Params);
		
	ElsIf Action = Actions.SetRecoveryModelToSimple Then
		
		SetRecoveryModelToSimple(Params);
		
	ElsIf Action = Actions.PermitDirtyRead Then
		
		PermitDirtyRead(Params);
		
	ElsIf Action = Actions.ConfigurationRepositoryUnbindCfg Then
		
		ConfigurationRepositoryUnbindCfg(Params);
					
	ElsIf Action = Actions.DumpConfigurationFromDatabaseToFile Then 
		
		DumpConfigurationFromDatabaseToFile(Params);
		
	ElsIf Action = Actions.LoadConfigurationFromFile Then
		
		LoadConfigurationFromFile(Params);	
		
	ElsIf Action = Actions.ConfigurationRepositoryBindCfg Then
		
		ConfigurationRepositoryBindCfg(Params);
		
	ElsIf Action = Actions.ConfigurationRepositoryUpdateCfg Then
		
		ConfigurationRepositoryUpdateCfg(Params);
					
	ElsIf Action = Actions.UpdateDBCfg Then
		
		UpdateDBCfg(Params);			
		
	ElsIf Action = Actions.ClearObjectVersions Then
		
		ClearObjectVersions(Params);			
				
	Else
					
		Raise StrTemplate(NStr("ru = 'Неподдерживаемое действие %1'; en = 'Unsupported action %1'"), Action);
		
	EndIf;
	
	
	//If Action = Actions.StoreSettingsToFile Then 
	//	
	//	StoreSettingsToFile(Params);
		
	//ElsIf Action = Actions.ClearObjectVersions Then
	//	
	//	ClearObjectVersions(Params);
	
	//ElsIf Action = Actions.InitialiseTestDatabase Then
	//	
	//	InitialiseTestDatabase(Params);
	
	
	
EndProcedure


Function  NewParams(Step)
	
	Action = Step.Action; 
	
	Common1CLogin = Constants.Common1CLogin.Get();
	Common1CPassword = Constants.Common1CPassword.Get();
	
	TargetLogin1C = ?(Not ValueIsFilled(Step.TargetDatabase) Or Step.TargetDatabase.UseCommon1CUser, Common1CLogin, Step.TargetDatabase.Login1C);
	TargetPassword1C = ?(Not ValueIsFilled(Step.TargetDatabase) Or Step.TargetDatabase.UseCommon1CUser, Common1CPassword, Step.TargetDatabase.Password1C);
	
	SourceLogin1C = ?(Not ValueIsFilled(Step.SourceDatabase) Or Step.SourceDatabase.UseCommon1CUser, Common1CLogin, Step.SourceDatabase.Login1C);
	SourcePassword1C = ?(Not ValueIsFilled(Step.SourceDatabase) Or Step.SourceDatabase.UseCommon1CUser, Common1CPassword, Step.SourceDatabase.Password1C);
	
	
	
	Actions = Enums.Actions;
	Params = New Structure;
	Params.Insert("SuccessMessage", "");
	Params.Insert("ErrorMessage");
	
		
	If Action = Actions.BackupDatabaseToTempDir Then // backuping source database
		
		Params.Insert("Server", Step.SourceDatabase.ServerSQL.Instance);
		Params.Insert("User", Step.SourceDatabase.ServerSQL.Login);
		Params.Insert("Password", Step.SourceDatabase.ServerSQL.Password);
		Params.Insert("Database", Step.SourceDatabase.DatabaseNameSQL);
		Params.Insert("BackupFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + "from_" + Step.SourceDatabase.DatabaseNameSQL + "_to_" + Step.TargetDatabase.DatabaseNameSQL + ".bak");		
		
		Params.SuccessMessage = NStr("ru = 'Выполнено сохранение базы %1 с сервера %3 в файл %2'; en = 'Database %1 from server %3 was saved to file %2'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.BackupFile, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось выполнить сохранение базы %1 с сервера %3 в файл %2. Причина: '; en = 'Failed to save database %1 from server %3 to file %2. Cause:'");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.BackupFile, Params.Server) + Chars.LF;		
		
	ElsIf Action = Actions.RestoreDatabaseFromTempDir Then
		
		Params.Insert("Server", Step.TargetDatabase.ServerSQL.Instance);
		Params.Insert("User" , Step.TargetDatabase.ServerSQL.Login);
		Params.Insert("Password" , Step.TargetDatabase.ServerSQL.Password);		
		Params.Insert("Database", Step.TargetDatabase.DatabaseNameSQL);
		Params.Insert("BackupFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + "from_" + Step.SourceDatabase.DatabaseNameSQL + "_to_" + Step.TargetDatabase.DatabaseNameSQL + ".bak");
		
		Params.SuccessMessage = NStr("ru = 'Выполнено восстановление базы %1 на сервере %3 из файла %2'; en = 'Database %1 on server %3 was restored from file %2'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.BackupFile, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не выполнено восстановление базы %1 на сервере %3 из файла %2. Причина: '; en = 'Faild to restore database %1 on server %3 from file %2. Cause:'");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.BackupFile, Params.Server) + Chars.LF;
		
		
	ElsIf Action = Actions.DeleteBackupSqlFile Then
		
		Params.Insert("BackupFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + "from_" + Step.SourceDatabase.DatabaseNameSQL + "_to_" + Step.TargetDatabase.DatabaseNameSQL + ".bak");
		
		Params.SuccessMessage = NStr("ru = 'Удален файл %1'; en = 'File %1 was removed'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.BackupFile);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось удалить файл %1. Причина: '; en = 'Failed to delete file %1. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.BackupFile) + Chars.LF;				
		
		
	ElsIf Action = Actions.SetRecoveryModelToSimple Then
			
		Params.Insert("Server", Step.TargetDatabase.ServerSQL.Instance);
		Params.Insert("User" , Step.TargetDatabase.ServerSQL.Login);
		Params.Insert("Password" , Step.TargetDatabase.ServerSQL.Password);		
		Params.Insert("Database", Step.TargetDatabase.DatabaseNameSQL);
			
		Params.SuccessMessage = NStr("ru = 'Для базы %1 на сервере %2 выполнено переключение модели восстановления на простую'; en = 'Recovery model for base %1 on server %2 was set to simple'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось переключить модель восстановления на простую для базы %1 на сервере %2. Причина: '; en = 'Faild to set recovery model to Simple for database %1 on server %2. Cause:'");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;
		
		
	ElsIf Action = Actions.PermitDirtyRead Then
			
		Params.Insert("Server", Step.TargetDatabase.ServerSQL.Instance);
		Params.Insert("User" , Step.TargetDatabase.ServerSQL.Login);
		Params.Insert("Password" , Step.TargetDatabase.ServerSQL.Password);		
		Params.Insert("Database", Step.TargetDatabase.DatabaseNameSQL);
			
		Params.SuccessMessage = NStr("ru = 'Для базы %1 на сервере %2 разрешено грязное чтение'; en = 'Dirty read is permited for base %1 on server %2'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось разрешить грязное чтение для базы %1 на сервере %2. Причина: '; en = 'Faild to permit dirty read for database %1 on server %2. Cause:'");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;
				
		
	ElsIf Action = Actions.ShrinkDatabase Then		
		
		Params.Insert("Server", Step.TargetDatabase.ServerSQL.Instance);
		Params.Insert("User" , Step.TargetDatabase.ServerSQL.Login);
		Params.Insert("Password" , Step.TargetDatabase.ServerSQL.Password);		
		Params.Insert("Database", Step.TargetDatabase.DatabaseNameSQL);
		
		Params.SuccessMessage = NStr("ru = 'Выполнено сжатие базы %1 на сервере %2'; en = 'Database %1 on server %2 was shrinked'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось выполнить сжатие базы %1 на сервере %2. Причина: '; en = 'Failed to shrink database %1 on server %2. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;		
				
	ElsIf Action = Actions.ConfigurationRepositoryUnbindCfg Then
		
		Params.Insert("Server", Step.TargetDatabase.Server1C.ClusterAddress);
		Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
		Params.Insert("User" , TargetLogin1C);
		Params.Insert("Password" , TargetPassword1C);				
		Params.Insert("PathToExecutable1CFile",  Constants.PathToExecutable1CFile.Get());
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		// Params.Insert("CheckErrorCode", DeployPlan.Steps[StepNumber-1].LoadReleaseConfiguration Or DeployPlan.Steps[StepNumber-1].ConnectToRepository); // database must be disconnected from repository if we need to load release configuration or reconnect it to repository
		
		Params.SuccessMessage = NStr("ru = 'Выполнено отключение от хранилища базы %1 на сервере 1С %2'; en = 'Database %1 on server %2 was unbinded from configuration repository'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось выполнить отключение от хранилища базы %1 на сервере 1С %2. Причина: '; en = 'Failed to unbind database %1 on server %2 from configuration repository. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;				
		
	ElsIf Action = Actions.ConfigurationRepositoryBindCfg Then
		
		Params.Insert("Server", Step.TargetDatabase.Server1C.ClusterAddress);
		Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
		Params.Insert("User" , TargetLogin1C);
		Params.Insert("Password" , TargetPassword1C);				
		Params.Insert("PathToExecutable1CFile",  Constants.PathToExecutable1CFile.Get());
		
		Params.Insert("RepositoryPath", Step.TargetDatabase.StoragePath);
		Params.Insert("RepositoryUser", Step.TargetDatabase.StorageUser);
		Params.Insert("RepositoryPassword", Step.TargetDatabase.StoragePassword);
		
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		
		
		Params.SuccessMessage = NStr("ru = 'Выполнено подключение к хранилищу базы %1 на сервере 1С %2'; en = 'Database %1 on server %2 was binded to configuration repository'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось выполнить подключение к хранилищу базы %1 на сервере 1С %2. Причина: '; en = 'Failed to bind database %1 on server %2 to configuration repository. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;				
		
		
	ElsIf Action = Actions.ConfigurationRepositoryUpdateCfg Then
		
		Params.Insert("Server", Step.TargetDatabase.Server1C.ClusterAddress);
		Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
		Params.Insert("User" , TargetLogin1C);
		Params.Insert("Password" , TargetPassword1C);				
		Params.Insert("PathToExecutable1CFile",  Constants.PathToExecutable1CFile.Get());
		
		Params.Insert("RepositoryPath", Step.TargetDatabase.StoragePath);
		Params.Insert("RepositoryUser", Step.TargetDatabase.StorageUser);
		Params.Insert("RepositoryPassword", Step.TargetDatabase.StoragePassword);
		
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		
		Params.SuccessMessage = NStr("ru = 'Рабочая конфигурация базы %1 на сервере 1С %2 обновлена из хранилища'; en = 'Working configuration in database %1 on server %2 was updated from repository'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось обновить из хранилища рабочую конфигурацию базы %1 на сервере 1С %2. Причина: '; en = 'Failed to update working configuration reposifory in database %1 on server %2. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;						
		
		
	ElsIf Action = Actions.UpdateDBCfg Then
			
		Params.Insert("Server", Step.TargetDatabase.Server1C.ClusterAddress);
		Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
		Params.Insert("User" , TargetLogin1C);
		Params.Insert("Password" , TargetPassword1C);				
		Params.Insert("PathToExecutable1CFile",  Constants.PathToExecutable1CFile.Get());
		
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		
		Params.SuccessMessage = NStr("ru = 'Обновлена конфигурация базы %1 на сервере 1С %2'; en = 'Databae configuration in database %1 on server %2 was updated'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось обновить конфигурацию базы %1 на сервере 1С %2. Причина: '; en = 'Failed to update database configuration in database %1 on server %2. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;			
		
	ElsIf Action = Actions.DumpConfigurationFromDatabaseToFile Then // dumping from source database 
		
		Params.Insert("Server", Step.SourceDatabase.Server1C.ClusterAddress);	
		Params.Insert("Database", Step.SourceDatabase.DatabaseName1C);
		Params.Insert("User" , SourceLogin1C);
		Params.Insert("Password" , SourcePassword1C);						
		Params.Insert("PathToExecutable1CFile",  Constants.PathToExecutable1CFile.Get());
		Params.Insert("PathToCFFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + "from_" + Step.SourceDatabase.DatabaseNameSQL + "_to_" + Step.TargetDatabase.DatabaseNameSQL + ".cf");
		
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		
		Params.SuccessMessage = NStr("ru = 'Выполнено сохранение конфигурации базы %1 на сервере 1С %2 в файл %3'; en = 'Configuration from database %1 on server %2 was saved to file %3'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server, Params.PathToCFFile);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось сохранить конфигурацию базы %1 на сервере 1С %2 в файл %3. Причина: '; en = 'Failed to save configuration from database %1 on server %2 to file %3. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server, Params.PathToCFFile) + Chars.LF;	

	ElsIf Action = Actions.LoadConfigurationFromFile Then
		
		Params.Insert("Server", Step.TargetDatabase.Server1C.ClusterAddress);	
		Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
		Params.Insert("User" , TargetLogin1C);
		Params.Insert("Password" , TargetPassword1C);				
		Params.Insert("PathToExecutable1CFile",  Constants.PathToExecutable1CFile.Get());
		Params.Insert("PathToCFFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + "from_" + Step.SourceDatabase.DatabaseNameSQL + "_to_" + Step.TargetDatabase.DatabaseNameSQL + ".cf");
		
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		
		Params.SuccessMessage = NStr("ru = 'Выполнена загрузка конфигурации из файла %3 в базу %1 на сервере 1С %2 из файла %3'; en = 'Configuration from file %3 was loaded to database %1 on server %2'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server, Params.PathToCFFile);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось загрузить конфигурацию из файла %3 в базу %1 на сервере 1С %2. Возможно существуют незакрытые соединения с базой. Описание ошибки :'; en = 'Failed to load configuration from file %3 to database %1 on server %2. Maybe there are active connections to the database. Error description : '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server, Params.PathToCFFile) + Chars.LF;		
		
		
	ElsIf Action = Actions.DeleteBackupCfFile Then
		
		Params.Insert("PathToCFFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + "from_" + Step.SourceDatabase.DatabaseNameSQL + "_to_" + Step.TargetDatabase.DatabaseNameSQL + ".cf");
		
		Params.SuccessMessage = NStr("ru = 'Удален файл %1'; en = 'File %1 was removed'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.PathToCFFile);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось удалить файл %1. Причина: '; en = 'Failed to delete file %1. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.PathToCFFile) + Chars.LF;						
		
	ElsIf Action = Actions.ClearObjectVersions Then
		
		Params.Insert("ServerSQL", Step.TargetDatabase.ServerSQL.Instance);
		Params.Insert("UserSQL" , Step.TargetDatabase.ServerSQL.Login);
		Params.Insert("PasswordSQL" , Step.TargetDatabase.ServerSQL.Password);		
		Params.Insert("DatabaseSQL", Step.TargetDatabase.DatabaseNameSQL);
		
		
		Params.Insert("Server1C", Step.TargetDatabase.Server1C.ClusterAddress);
		Params.Insert("User1C" , TargetLogin1C);
		Params.Insert("Password1C" , TargetPassword1C);		
		Params.Insert("Database1C", Step.TargetDatabase.DatabaseName1C);
		
		Params.Insert("UnlockCode",  Constants.UnlockCodeFor1CDatabase.Get());
		
		Params.SuccessMessage = NStr("ru = 'Очищены таблицы версий объектов в базе %1 на сервере %2'; en = 'Object versions tables were cleared in database %1 on server %2'");
		Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.DatabaseSQL, Params.ServerSQL);
		
		Params.ErrorMessage = NStr("ru = 'Не удалось очистить таблицы версий объектов в базе %1 на сервере %2. Причина: '; en = 'Failed to clear object versions tables in database %1 on server %2. Cause: '");
		Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.DatabaseSQL, Params.ServerSQL) + Chars.LF;		
				
	Else
		
		Params.SuccessMessage = StrTemplate(NStr("ru = 'Неподдерживаемое действие %1'; en = 'Unsupported action %1'"), Action);
		Params.ErrorMessage = Params.SuccessMessage;
		
	EndIf;
		
	
	Return Params;
	
	
	
	// Drafts :
	
	//ElsIf Action = Actions.InitialiseTestDatabase Then
	//	
	//	Params.Insert("Server", Step.TargetDatabase.Server1C);
	//	Params.Insert("User" , Login1C));
	//	Params.Insert("Password" , Password1C);		
	//	Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
	//	Params.Insert("StartupParams",  "GP_InitializeTestDevDatabase");
	//	
	//	If DeployPlan.Steps[StepNumber-1].PreserveDatabaseSettings Then
	//		Params.Insert("SettingsFile", Constants.BackupSharedFolder.Get() + GetPathSeparator() + Step.TargetDatabase.DatabaseName1C + "_settings.xml");
	//	Else
	//		Params.Insert("SettingsFile", Undefined);
	//	EndIf;
	//	
	//	Params.SuccessMessage = NStr("ru = 'Выполнена инициализация базы %1 на сервере 1С %2 как базы для тестирования или разработки'; en = 'Database %1 on server %2 was initialized as test database or develop database'");
	//	Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.Server);
	//	
	//	Params.ErrorMessage = NStr("ru = 'Не удалось выполнить инициализацию базы %1 на сервере 1С %2 как базы для тестирования или разработки. Причина: '; en = 'Failed to initialize database %1 on server %2 as test database or develop database. Cause: '");
	//	Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.Server) + Chars.LF;			
	
	
	//If Action = Actions.StoreSettingsToFile Then
	//														   
	//	Params.Insert("Server", Step.TargetDatabase.Server1C);
	//	Params.Insert("User" , Login1C);
	//	Params.Insert("Password" , Password1C);		
	//	Params.Insert("Database", Step.TargetDatabase.DatabaseName1C);
	//	Params.Insert("SettingsFile",  Constants.BackupSharedFolder.Get() + GetPathSeparator() + Step.TargetDatabase.DatabaseName1C + "_settings.xml");
	//	
	//	Params.SuccessMessage = NStr("ru = 'Настройки базы %1 на сервере %3 сохранены в файл %2'; en = 'Settings for database %1 on server %3 was stored to file %2'");
	//	Params.SuccessMessage = StrTemplate(Params.SuccessMessage, Params.Database, Params.SettingsFile, Params.Server);
	//	
	//	Params.ErrorMessage = NStr("ru = 'Не удалось сохранить настройки базы %1 на сервере %3 в файл %2. Причина: '; en = 'Faild store settings for database %1 on server %3 to file %2. Cause:'");
	//	Params.ErrorMessage = StrTemplate(Params.ErrorMessage, Params.Database, Params.SettingsFile, Params.Server) + Chars.LF;		
	//	
	
	
EndFunction


Function CopyRow(TargetTable, SourceRow)
	
	NewRow = TargetTable.Add();
	FillPropertyValues(NewRow, SourceRow);
	Return NewRow;
	
EndFunction


Function FormStepsToExecuteByPlanSteps(PlanSteps) 
	
	
	#If _Dev_ Then
		PlanSteps = New ValueTable;
	#EndIf
	
	Actions = Enums.Actions;
	StepsToExecute = PlanSteps.CopyColumns();
	StepsToExecute.Columns.Add("Params");
	
	AddedActionConfigurationRepositoryUnbindCfg = False;
	
	
	
	//UserAvailableActions.Add(Actions.DenySessions);
	//UserAvailableActions.Add(Actions.DenyScheduledJobs);
	//UserAvailableActions.Add(Actions.TerminateSessions);
	//UserAvailableActions.Add(Actions.PermitSessions);
	//UserAvailableActions.Add(Actions.PermitScheduledJobs);
	
	
	For Each PlanStep In PlanSteps Do 
		
		If PlanStep.Action = Actions.CopyTargetDatabaseFromSourceDatabase Then
						
			// decomposing on steps : dump, restore, delete backup file, disconnect from storage
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.BackupDatabaseToTempDir;
			StepToExecute.Params = NewParams(StepToExecute);			
						
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.RestoreDatabaseFromTempDir;
			StepToExecute.Params = NewParams(StepToExecute);			
		
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.DeleteBackupSqlFile;
			StepToExecute.Params = NewParams(StepToExecute);			
			
			If Not AddedActionConfigurationRepositoryUnbindCfg Then
			
				StepToExecute = CopyRow(StepsToExecute, PlanStep);
				StepToExecute.Action = Actions.ConfigurationRepositoryUnbindCfg;
				StepToExecute.Params = NewParams(StepToExecute);
				
				AddedActionConfigurationRepositoryUnbindCfg = True;
				
			EndIf;	
				
		ElsIf PlanStep.Action = Actions.SetRecoveryModelToSimple Then
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);			
			StepToExecute.Params = NewParams(StepToExecute);			
			
		ElsIf PlanStep.Action = Actions.PermitDirtyRead Then
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);			
			StepToExecute.Params = NewParams(StepToExecute);			
						
			
		ElsIf PlanStep.Action = Actions.ShrinkDatabase Then
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);			
			StepToExecute.Params = NewParams(StepToExecute);			
			
		ElsIf PlanStep.Action = Actions.ConfigurationRepositoryBindCfg Then
			
			
			// decomposing on steps: disconnect from storage, bind to sotrage, update configuration from storage, update database configuration
			
			If Not AddedActionConfigurationRepositoryUnbindCfg Then
			
				StepToExecute = CopyRow(StepsToExecute, PlanStep);
				StepToExecute.Action = Actions.ConfigurationRepositoryUnbindCfg;
				StepToExecute.Params = NewParams(StepToExecute);
				
				AddedActionConfigurationRepositoryUnbindCfg = True;
				
			EndIf;				
						
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.ConfigurationRepositoryBindCfg;
			StepToExecute.Params = NewParams(StepToExecute);			
						
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.ConfigurationRepositoryUpdateCfg;
			StepToExecute.Params = NewParams(StepToExecute);			
		
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.UpdateDBCfg;
			StepToExecute.Params = NewParams(StepToExecute);			
			
		ElsIf PlanStep.Action = Actions.LoadConfigurationFromSourceDatabase Then
			
			// decomposing on steps: disconnect from storage, dump cf to file , load cf from file (with UpdateDBCfg option)
			
			If Not AddedActionConfigurationRepositoryUnbindCfg Then
			
				StepToExecute = CopyRow(StepsToExecute, PlanStep);
				StepToExecute.Action = Actions.ConfigurationRepositoryUnbindCfg;
				StepToExecute.Params = NewParams(StepToExecute);
				
				AddedActionConfigurationRepositoryUnbindCfg = True;
				
			EndIf;				
						
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.DumpConfigurationFromDatabaseToFile;
			StepToExecute.Params = NewParams(StepToExecute);			
						
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.LoadConfigurationFromFile;
			StepToExecute.Params = NewParams(StepToExecute);			
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);
			StepToExecute.Action = Actions.DeleteBackupCfFile;
			StepToExecute.Params = NewParams(StepToExecute);						
			
		ElsIf PlanStep.Action = Actions.ClearObjectVersions Then
			
			StepToExecute = CopyRow(StepsToExecute, PlanStep);			
			StepToExecute.Params = NewParams(StepToExecute);									
						
		EndIf;
					
		
		//	If Row.PreserveDatabaseSettings Then
		//		LastItem = NewActionItem(Actions.StoreSettingsToFile, DeployPlan, Row.LineNumber, LastItem, FirstItem);			
		//	EndIf;	
		//	If Row.InitializeAsTestDatabase Then			
		//		LastItem = NewActionItem(Actions.InitialiseTestDatabase, DeployPlan, Row.LineNumber, LastItem, FirstItem);
		//	EndIf;	
		
		
				
	EndDo;
	                     
	Return StepsToExecute;
		
 
		                 
	
EndFunction


Procedure LogMessage(DeployPlanRef, StartTime, BackgroundJobUUID, Success, Message)

	LogRecordManager = InformationRegisters.DeployPlansLog.CreateRecordManager();	
	LogRecordManager.DeployPlan = DeployPlanRef;
	LogRecordManager.StartTime = StartTime;
	LogRecordManager.Time = CurrentDate();
	LogRecordManager.ProcessedByClient = False;
	LogRecordManager.BackgroundJobID = BackgroundJobUUID;
	LogRecordManager.UniversalTimeInMilliseconds = CurrentUniversalDateInMilliseconds();
	LogRecordManager.Message = Message;
	LogRecordManager.Success = Success;
	LogRecordManager.Write();
	
EndProcedure


Procedure LogStepResult(DeployPlanRef, StartTime, BackgroundJobUUID, Step, ExceptionMessage)

	Success = Not ValueIsFilled(ExceptionMessage);
	
	LogMessage(DeployPlanRef, StartTime, BackgroundJobUUID, Success, ?(Success, Step.Params.SuccessMessage, Step.Params.ErrorMessage + ExceptionMessage));
	
EndProcedure



Procedure ExecuteListOfActions(DeployPlanRef, PlanSteps, DebubMode = False) Export
	
	
	CurrentBackgroundJob = GetCurrentInfoBaseSession().GetBackgroundJob();
	StartTime = CurrentDate();
	
	If DebubMode Then		
		BackgroundJobID = SessionParameters.SessionID;		
	Else		
		If CurrentBackgroundJob = Undefined Then
			Return;
		EndIf;
	
		BackgroundJobID = CurrentBackgroundJob.UUID;		
	EndIf;
		
	
	
	StateRecordManager = InformationRegisters.DeployPlansStates.CreateRecordManager();			
	StateRecordManager.Period = StartTime;
	StateRecordManager.DeployPlan = DeployPlanRef;		
	StateRecordManager.SessionID = SessionParameters.SessionID;	
	StateRecordManager.BackgroundJobID = BackgroundJobID;		
	StateRecordManager.Write();
		
	
	Success = True;
	
	Try
		StepsToExecute = FormStepsToExecuteByPlanSteps(PlanSteps);
		Message = NStr("en = 'Formed list of actions. Process started.'; ru = 'Сформирован список действий. Процесс начат.'");
		LogMessage(DeployPlanRef, StartTime, BackgroundJobID, True, Message);
	Except
		Success = False;
		ErrorMessage = ErrorDescription();
		LogMessage(DeployPlanRef, StartTime, BackgroundJobID, False, ErrorMessage);
	EndTry;
	
	If Success Then
	
		If StepsToExecute.Count() = 0 Then 
			Success = False;
			Message = NStr("en = 'There are not steps to execute.'; ru = 'Отсутствуют шаги для исполнения.'");
			LogMessage(DeployPlanRef, StartTime, BackgroundJobID, False, Message);
		EndIf;	
	
	EndIf;	
	
	
	If Success Then
	
		For Each Step In StepsToExecute Do		
			
			ExceptionMessage = "";
			
			Try
				Message = StrTemplate(NStr("en = 'Action started: %1'; ru = 'Выполняется действие: %1'"), Step.Action);
				LogMessage(DeployPlanRef, StartTime, BackgroundJobID, True, Message);
				ExecuteStep(Step);			
			Except
				ExceptionMessage = ErrorDescription();
				Success = False;	
			EndTry;
			
			
			LogStepResult(DeployPlanRef, StartTime, BackgroundJobID, Step, ExceptionMessage);
			
			
			If Not Success Then
				Break;
			EndIf;
								
		EndDo;	
		
	EndIf;	
	
	
	
	If Success Then		
		Message = NStr("ru = 'Выполнение успешно завершено'; en = 'Process successfully finished '");
		LogMessage(DeployPlanRef, StartTime, BackgroundJobID, True, Message);
	Else	
		Message = NStr("ru = 'Во время выполнения произошла ошибка'; en = 'An error occurred while running plan'");
		LogMessage(DeployPlanRef, StartTime, BackgroundJobID, False, Message);
	EndIf;		
	
	
	
	StateRecordManager.EndTime = CurrentDate();
	StateRecordManager.Write();
				
	
EndProcedure


Function ExecutePlanInBackgroundJob(DeployPlanRef, PlanStepsAddress, DebubMode = False) Export
	
	PlanSteps = GetFromTempStorage(PlanStepsAddress);
	
	If PlanSteps.Columns.Find("Execute") <> Undefined Then
		PlanSteps = PlanSteps.Copy(New Structure("Execute", True));
	EndIf;
	
	JobParams = New Array;
	JobParams.Add(DeployPlanRef);
	JobParams.Add(PlanSteps);
	
	BackgroundJobID = Undefined;
	
	If DebubMode Then			
		ExecuteListOfActions(DeployPlanRef, PlanSteps, DebubMode);
		BackgroundJobID = SessionParameters.SessionID;
	Else
		Job = BackgroundJobs.Execute("Processor.ExecuteListOfActions", JobParams, SessionParameters.SessionID); 
		BackgroundJobID = Job.UUID;
	EndIf;
		
	Return BackgroundJobID;
	
EndFunction


Function TerminatePlanExecution(BackgroundJobID) Export
	
	Job = BackgroundJobs.FindByUUID(New UUID(BackgroundJobID));
	
	If Job <> Undefined And Job.State = BackgroundJobState.Active Then
		Job.Cancel();
	EndIf;
		
	Query = New Query;
	Query.Text = 
	"SELECT TOP 1
	|	DeployPlansStatesSliceLast.Period,
	|	DeployPlansStatesSliceLast.DeployPlan AS DeployPlan,
	|	DeployPlansStatesSliceLast.SessionID AS SessionID,
	|	DeployPlansStatesSliceLast.BackgroundJobID AS BackgroundJobID,
	|	DeployPlansStatesSliceLast.EndTime AS EndTime
	|FROM
	|	InformationRegister.DeployPlansStates.SliceLast(, BackgroundJobID = &BackgroundJobID) AS DeployPlansStatesSliceLast
	|WHERE
	|	DeployPlansStatesSliceLast.EndTime = DATETIME(1, 1, 1)";
	
	Query.SetParameter("BackgroundJobID", BackgroundJobID);
	
	QueryResult = Query.Execute();
	If Not QueryResult.IsEmpty() Then
		
		Selection = QueryResult.Select();
		Selection.Next();
		
		StateRecordManager = InformationRegisters.DeployPlansStates.CreateRecordManager();			
		FillPropertyValues(StateRecordManager, Selection);		
		StateRecordManager.EndTime = CurrentDate();
		StateRecordManager.Write();
			
		Message = NStr("ru = 'Выполнение плана прервано'; en = 'Plan execution was terminated'");
		LogMessage(Selection.DeployPlan, Selection.Period, BackgroundJobID, False, Message);
		
	EndIf;		
	
	
	Return Job <> Undefined;	
	
EndFunction



Function LogRecordsByBackgroundJobID(BackgroundJobID, OnlyUnprocessed = True, MarkAsProcessed = True) Export
	
	Query = New Query;
	Query.Text = 
		"SELECT
		|	*
		|FROM
		|	InformationRegister.DeployPlansLog AS DeployPlansLog
		|WHERE
		|	DeployPlansLog.BackgroundJobID = &BackgroundJobID
		|	AND (NOT &OnlyUnprocessed
		|			OR NOT DeployPlansLog.ProcessedByClient)";
	
	Query.SetParameter("BackgroundJobID", BackgroundJobID);	
	Query.SetParameter("OnlyUnprocessed", OnlyUnprocessed);	
	
	QueryResult = Query.Execute();
	LogRecords = QueryResult.Unload();
	
	If MarkAsProcessed Then
		
		For Each Row In LogRecords Do
		    If Not Row.ProcessedByClient Then
				LogRecordManager = InformationRegisters.DeployPlansLog.CreateRecordManager();	
				FillPropertyValues(LogRecordManager, Row);
				LogRecordManager.ProcessedByClient = True;
				LogRecordManager.Write();
			EndIf;
		EndDo;
		
	EndIf;
	
	Return LogRecords;	
	
EndFunction



